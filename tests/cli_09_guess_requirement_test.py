import filecmp
import unittest
from pathlib import Path

from . import cli_runner_invoke, move_to_test_folder


class TestCliGuessRequirement(unittest.TestCase):
    def test_cli_guess_requirement(self):
        move_to_test_folder()

        expected_folder_path = Path("../output_expected").absolute()

        cli_runner_invoke(
            ["guess-requirement", "--extra-modules=sentry"],
        )

        relative_path = Path("src/env_14.0/addons_python_requirements.txt")

        assert filecmp.cmp(
            relative_path,
            expected_folder_path / relative_path,
        )
