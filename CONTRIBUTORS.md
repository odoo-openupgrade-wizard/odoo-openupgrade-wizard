# Developers

* Sylvain LE GAL, from [GRAP](http://www.grap.coop), since March 2022
* Rémy TAYMANS, from [Coop It Easy](https://coopiteasy.be/), since June 2022

* Cyril JEANNERET, from [Camptocamp](https://www.camptocamp.com), since April 2023
* Simon MAILLARD, form [Ogesta](https://ogesta.fr/), since July 2023

* Hugues DE KEYSER, from [Coop It Easy](https://coopiteasy.be/), since April 2024
* Gabriel PICKENHAYN, since May 2024
* Boris GALLET, since September 2024
* Ahmet YIĞIT BUDAK, from [Altinkaya](https://www.altinkaya.com/fr), since October 2024
* Alexandre AUBIN, from [Coopaname](https://www.coopaname.coop/), since October 2024

# Reviewers

* Sébastien BEAU, from Akretion (https://akretion.com)
