Add an option ``update`` in the configuration file at migration_step level.
If disabled, the update=all step will be skipped during this step.
That can be interesting to save time, during the first and the last steps of the migration
process (when ``execution_context='regular').
